jQuery(document).ready(function() {
      var owl = jQuery("#owl-demo");
      owl.owlCarousel({
    autoPlay: 5000, //Set AutoPlay to 3 seconds 
 
      items : 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,1],
	  itemsTablet : [768, 1],
        itemsMobile : [479, 1],navigation: true,
          
});
});
jQuery(document).ready(function() {
      var owl = jQuery("#owl-demo2");
      owl.owlCarousel({
    autoPlay: 5000, //Set AutoPlay to 3 seconds 
 
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,2],
	  itemsTablet : [768, 2],
        itemsMobile : [479, 1],
        navigation: true,
    
});
});

jQuery(document).ready(function() {
      var owl = jQuery("#owl-demo3");
      owl.owlCarousel({
    autoPlay: 5000, //Set AutoPlay to 3 seconds 
 
      items : 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,1],
	  itemsTablet : [768, 1],
        itemsMobile : [479, 1],
        navigation: true,
    
       autoplayTimeout:2000,
        autoplayHoverPause:true,
});
});



/*-------------------------------------
         Jquery Serch Box
-------------------------------------*/

$(".search-icon").on("click",function(b){
            var a=$(".inputtxt").hasClass("expanded")?"0px":"257px";
$(".inputtxt").stop().animate({width:a},300,function(){$(this).toggleClass("expanded skin")}
)
 });

// hiddenBarToggler
function hiddenBarToggler () {
  var subMenu = $ (".aside-menu-wrapper ul li.dropdown-holder>a"),
      expender = $ (".aside-menu-wrapper ul li.dropdown-holder .expander");

    if ($('.close-aside-menu').length) {
      $('.close-aside-menu').on('click', function () {
        $('#hidden-aside-menu').removeClass("show-menu");
      });
    };
    if ($('.toggle-show-menu-button').length) {
      $('.toggle-show-menu-button').on('click', function () {
        $('#hidden-aside-menu').addClass("show-menu");
      });
    };

    subMenu.on("click", function (e) {
        e.preventDefault();
    });

    subMenu.append(function () {
      return '<button type="button" class="expander"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>';
    });
    subMenu.on('click', function () {
      $(this).parent('li').children('ul').slideToggle();
    });
}

// customScrollBarHiddenSidebar
function customScrollBar () {
  var Cscroll = $("#hidden-aside-menu");
  if  (Cscroll.length) {
        Cscroll.mCustomScrollbar();
  };
}
// scroll top 
$("#back-top").hide();
$(function () {
$(window).scroll(function () {
var footerscroll = $(".statenews-scroll").offset().top;
    
if ($(this).scrollTop() > footerscroll) {
$('#back-top').fadeIn();
} else {
$('#back-top').fadeOut(); }});
$('#back-top a').click(function () {
$('body,html').animate({
scrollTop: 0
}, 600); return false; });
});



// DOM ready function
jQuery(document).on('ready', function() {
	(function ($) {     
     hiddenBarToggler ();
     customScrollBar ();     
  })(jQuery);
});


/*-------------------------------------
         Jquery page scroll
         -------------------------------------*/



$(".topsection-scroll").click(function () {
					$('html,body').animate({
						scrollTop: $(".top-section").offset().top-120},'slow');
                   
                    
				});
$(".four-column-scroll").click(function () {
					$('html,body').animate({
						scrollTop: $(".four-column-block").offset().top-70},'slow');
                   
                    
				});
    $(".about-scroll").click(function () {
					$('html,body').animate({
						scrollTop: $(".aboutus-txt").offset().top-50},
						'slow');
        
				});
     $(".upcoming-scroll").click(function () {
					$('html,body').animate({
						scrollTop: $(".upcoming-event").offset().top-85},
						'slow');
         
     });
$(".statenews-scroll").click(function () {
					$('html,body').animate({
						scrollTop: $(".state-news").offset().top-120},
						'slow');
         
     });


jQuery(document).on('ready', function() {
 var a=$(window).height();
     var b=$(window).width();
    //alert(b);
 $('.home-slider .owl-item,.home-slider .item,.home-slider .item img').css({'height':a})
});






$(document).ready(function() {
        $('#font-in').click(function(){	   
        curSize= parseInt($('.top-section p').css('font-size')) + 1;
		if(curSize<=23)
        $('.top-section p').css('font-size', curSize);
            
        curSize2= parseInt($('h3').css('font-size')) + 1;
		if(curSize2<=27)
        $('h3').css('font-size', curSize2);
            
        curSize3= parseInt($('.box-btmbg h2').css('font-size')) + 1;
		if(curSize3<=33)
        $('.box-btmbg h2').css('font-size', curSize3);
            
        curSize4= parseInt($('h1').css('font-size')) + 1;
		if(curSize4<=42)
        $('h1').css('font-size', curSize4);
            
        });
            
		$('#font-dec').click(function(){	   
        curSize= parseInt($('.top-section p').css('font-size')) - 1;
		if(curSize>=17)
        $('.top-section p').css('font-size', curSize);
            
        curSize2= parseInt($('h3').css('font-size')) - 1;
		if(curSize2>=20)
        $('h3').css('font-size', curSize2);
            
        curSize3= parseInt($('.box-btmbg h2').css('font-size')) - 1;
		if(curSize3>=24)
        $('.box-btmbg h2').css('font-size', curSize3);
            
        curSize4= parseInt($('h1').css('font-size')) - 1;
		if(curSize4>=38)
        $('h1').css('font-size', curSize4);           
            
        });
         
         $('#font-df').click(function(){
        $('.top-section p').css('font-size', 20);
         $('h3').css('font-size', 24); 
        $('.box-btmbg h2').css('font-size', 30);
        $('h1').css('font-size', 40);
        });
            
            
        });


$('#color-theme-default').css({
            'background-color': '#222222'
        });
        $('#color-theme-light').css({
            'background-color': '#a0140c'
        });
        $('#color-theme-dark').css({
            'background-color': '#13a2e4'
        });
        $('.color-selector-wrapper a').click(function() {
            var color = $(this).css('background-color');
            $('header,footer').css('background-color', color);
            if (typeof(Storage) !== 'undefined') {
                // Store
                localStorage.setItem('colorvalue', color);
            } else {
                $('#result').html('Sorry, your browser does not support Web Storage...!');
            }
        });
        $(document).ready(function() {
            if (typeof(Storage) !== 'undefined') {
                // Retrieve
                var color = localStorage.getItem('colorvalue');
                $('header,footer').css('background-color', color);
            } else {
                $('#result').html('Sorry, your browser does not support Web Storage...!');
            }
        });
  